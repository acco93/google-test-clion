//
// Created by acco on 5/20/18.
//

#include <gtest/gtest.h>
#include <gmock/gmock.h>


#include "../src/ClassName.hpp"

using testing::Eq;

namespace {

    class BasicTests : public testing::Test {

    public:

        ClassName obj;

        BasicTests() {

            // perform some setup if needed
            obj;
        }
    };

    TEST_F(BasicTests, testName0) {

        obj.setValue(10);
        ASSERT_EQ(10, obj.getValue());

    }

    TEST_F(BasicTests, testName1) {

        obj.setValue(11);
        ASSERT_THAT(10, testing::Eq(obj.getValue()));

    }


}