//
// Created by acco on 5/21/18.
//

#ifndef GOOGLETESTSETUP_CLASSNAME_HPP
#define GOOGLETESTSETUP_CLASSNAME_HPP


class ClassName {

    int value;

public:
    int getValue() const {
        return value;
    }

    void setValue(int value) {
        ClassName::value = value;
    }

};


#endif //GOOGLETESTSETUP_CLASSNAME_HPP
